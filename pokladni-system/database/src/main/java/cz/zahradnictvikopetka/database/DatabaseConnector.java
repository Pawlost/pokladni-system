package cz.zahradnictvikopetka.database;

import java.sql.ResultSet;
import java.sql.SQLException;

interface DatabaseConnector {
    ResultSet query(String sql, String... data) throws SQLException;
    boolean execute(String sql, String... data) throws SQLException;
}
