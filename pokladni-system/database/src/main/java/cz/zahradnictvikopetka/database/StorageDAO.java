package cz.zahradnictvikopetka.database;

import java.sql.ResultSet;
import java.sql.SQLException;

class StorageDAO {
    private final DatabaseConnector databaseConnector;

    public StorageDAO(DatabaseConnector databaseConnector) {
        this.databaseConnector = databaseConnector;
    }

    public ResultSet getAllItems() throws SQLException {
        return databaseConnector.query("SELECT * FORM items");
    }

    public boolean insertNewItem(String ItemData) throws SQLException {
        return databaseConnector.execute("INSERT INTO items() VALUES ?",ItemData);
    }

}
