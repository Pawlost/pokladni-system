package cz.zahradnictvikopetka.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

public class MysqlDatabaseConnector implements DatabaseConnector{

    private final Connection databaseConnection;
    private final String ip;
    private final String port;
    private final String username;
    private final String password;
    private final String database;

    public MysqlDatabaseConnector(String ip, String port, String username, String password, String database) throws SQLException, ClassNotFoundException {
        Class.forName("com.mysql.jdbc.Driver");
        this.ip = ip;
        this.port = port;
        this.username = username;
        this.password = password;
        this.database = database;
        databaseConnection = DriverManager.getConnection(String.format("jdbc:mysql://%s:%s/%s", this.ip, this.port, this.database), username, password);
    }

    public ResultSet query(String sql, String... data) throws SQLException {
        return databaseConnection.prepareStatement(sql, data).executeQuery();
    }

    public boolean execute(String sql, String... data) throws SQLException {
        return databaseConnection.prepareStatement(sql, data).execute();
    }

}
